# Структура папок и файлов

```text
ninelines-template
├── src
│   ├── images
│   │   └── sprites
│   │       ├── png
│   │       │   └── .keep
│   │       └── svg
│   │           └── .keep
│   ├── js
│   │   ├── vendor
│   │   │   └── .keep
│   │   ├── main.js
│   │   └── vendor.js
│   ├── pug
│   │   ├── mixins
│   │   │   └── svg.pug
│   │   └── base.pug
│   ├── resources
│   │   └── fonts
│   ├── sass
│   │   ├── functions
│   │   │   └── _sprites.sass
│   │   ├── mixins
│   │   │   ├── _sprites.sass
│   │   ├── vendor
│   │   │   └── .keep
│   │   ├── _base.sass
│   │   ├── _functions.sass
│   │   ├── _mixins.sass
│   │   ├── _sprites.hbs
│   │   ├── _sprites.sass
│   │   ├── _variables.sass
│   │   ├── _vendor.sass
│   │   └── main.sass
│   └── index.pug
├── .babelrc
├── .editorconfig
├── .gitignore
├── bitbucket-pipelines.yml
├── gulpfile.js
├── package.json
└── README.md
```

## `src`

В папке `src` хранятся исходные файлы проекта.

## `src/images`

Папка `images` предназначена для хранения изображений.
При сборке файлы из данной папки попадают в `build/images`.

## `src/images/sprites`

Папка `src/images/sprites` предназначена для хранения векторных (SVG) и растровых (PNG) иконок.

## `src/images/sprites/png`

Папка `src/images/sprites/png` предназначена для хранения растровых иконок.
При сборке файлы из данной папки объединяются в два спрайта: `build/images/sprites.png` и `build/images/sprites@2x.png`.

## `src/images/sprites/svg`

Папка `src/images/sprites/svg` предназначена для хранения векторных иконок.
При сборке файлы из данной папки объединяются в один спрайт: `build/images/sprites.svg`.

## `src/js`

Папка `src/js` предназначена для хранения скриптов.

## `src/js/vendor`

Папка `src/js/vendor` предназначена для хранения скриптов сторонних библиотек, которых нет в репозитории npm.

## `src/js/main.js`

Файл `src/js/main.js` предназначен для хранения основной логики сайта.
При сборке данный файл попадает в `build/js`.

## `src/js/vendor.js`

Файл `src/js/vendor.js` предназначен для подключения сторонних библиотек.

При сборке данный файл попадет в `build/js`.

## `src/pug`

Папка `src/pug` предназначена для хранения шаблонов.

## `src/pug/mixins`

Папка `src/pug/mixins` предназначена для хранения Pug-миксинов.

## `src/pug/mixins/svg.pug`

В файле `src/pug/mixins/svg.pug` хранится Pug-миксин для подключения SVG-иконок.

## `src/pug/base.pug`

В файле `src/pug/base.pug` хранится базовый шаблон страниц сайта.

## `src/resources`

Папка `src/resources` предназначена для хранения различных файлов проекта.
При сборке файлы из данной папки попадают в `build`.

## `src/resources/fonts`

Папка `src/resources/fonts` предназначена для хранения шрифтов.
При сборке файлы из данной папки попадают в `build/fonts`.

## `src/sass`

Папка `src/sass` предназначена для хранения стилей.

## `src/sass/functions`

Папка `src/sass/functions` предназначена для хранения sass-функций.

## `src/sass/functions/_sprites.sass`

В файле `src/sass/functions/_sprites.sass` хранятся sass-функции для работы с PNG-спрайтами.

## `src/sass/mixins`

Папка `src/sass/mixins` предназначена для хранения sass-миксинов.

## `src/sass/mixins/_sprites.sass`

## `src/sass/vendor`

Папка `src/sass/vendor` предназначена для хранения стилей сторонних библиотек, которых нет в репозитории npm.

## `src/sass/_base.sass`

Файл `src/sass/_base.sass` предназначен для хранения базовых стилей.

## `src/sass/_fonts.sass`

Файл `src/sass/_fonts.sass` предназначен для подключения шрифтов.

## `src/sass/_functions.sass`

Файл `src/sass/_functions.sass` предназначен для подключения функций из папки `src/sass/functions`.

## `src/sass/_mixins.sass`

Файл `src/sass/_mixins.sass` предназначен для подключения миксинов из папки `src/sass/mixins`.

## `src/sass/_sprites.hbs`

`src/sass/_sprites.hbs` — шаблон, на основе которого генерируется содержимое файла `src/sass/_sprites.sass`.

## `src/sass/_sprites.sass`

Файл `src/sass/_sprites.sass` предназначен для работы с PNG-спрайтами.
Содержимое данного файла автоматически генерируется на основе шаблона `src/sass/_sprites.hbs` и иконок из папки `src/images/sprites/png`.

## `src/sass/_variables.sass`

Файл `src/sass/_variables.sass` предназначен для хранения sass-переменных.

## `src/sass/_vendor.sass`

Файл `src/sass/_vendor.sass` предназначен для подключения стилей сторонних библиотек.

## `src/sass/main.sass`

Файл `src/sass/main.sass` предназначен для хранения основных стилей сайта.
При сборке данный файл преобразуется в CSS и сохраняется в `build/css` вместе с файлом `main.css.map`.

## `src/index.pug`

`src/index.pug` — шаблон главной страницы.
При сборке все Pug-файлы из папки `src` преобразуются в HTML и сохраняются в `build`.

## `.babelrc`

`.babelrc` — файл настроек JavaScript-транспайлера Babel.

## `.editorconfig`

`.editorconfig` — файл настроек редактора.

## `.npmrc`

`.npmrc` — файл настроек npm.

## `bitbucket-pipelines.yml`

`bitbucket-pipelines.yml` — файл настроек Bitbucket Pipelines.

## `gulpfile.js`

`gulpfile.js` — основной файл сборки, содержащий Gulp-задачи.

## `package.json`

`package.json` — файл, содержащий базовую информацию о проекте и список требуемых библиотек.

## `README.md`

`README.md` — описание проекта.